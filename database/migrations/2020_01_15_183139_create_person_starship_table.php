<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonStarshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_starship', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('person_id')->unsigned()->index();
            $table->bigInteger('starship_id')->unsigned()->index();
            $table->timestamps();
        });

        Schema::table('person_starship', function ($table) {
            $table->foreign('person_id')->references('id')->on('people');
            $table->foreign('starship_id')->references('id')->on('starships');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person_starship');
    }
}
