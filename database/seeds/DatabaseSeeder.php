<?php

use App\Http\Traits\StarWarsTrait;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    use StarWarsTrait;
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->getSwapiData();
    }
}
