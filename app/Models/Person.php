<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $fillable = [
        'name',
        'height',
        'mass',
        'hair_color',
        'skin_color',
        'eye_color',
        'birth_year',
        'gender',
        'homeworld',
        'created',
        'edited',
        'url',
    ];

    public function films()
    {
        return $this->belongsToMany('App\Models\Film', 'film_person', 'person_id', 'film_id');
    }

    public function species()
    {
        return $this->belongsToMany('App\Models\Species', 'person_species', 'person_id', 'species_id');
    }

    public function starships()
    {
        return $this->belongsToMany('App\Models\Starship', 'person_starship', 'person_id', 'starship_id');
    }

    public function vehicles()
    {
        return $this->belongsToMany('App\Models\Vehicle', 'person_vehicle', 'person_id', 'vehicle_id');
    }
}
