<?php

namespace App\Http\Services;

use App\Http\Requests\PersonShowRequest;
use App\Models\Person;

class PersonService
{
    public function personDataByName(PersonShowRequest $request)
    {
        $data = $request->validated();
        $person = Person::with('films')
            ->with('species')
            ->with('starships')
            ->with('vehicles')
            ->where('people.name', $data['name'])
            ->first();

        return $person;
    }
}