<?php

namespace App\Http\Resources;

use App\Models\Species;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SpeciesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Species $species) {
            return (new SpeciesResource($species));
        });

        return parent::toArray($request);
    }
}
