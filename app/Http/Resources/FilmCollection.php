<?php

namespace App\Http\Resources;

use App\Models\Film;
use Illuminate\Http\Resources\Json\ResourceCollection;

class FilmCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Film $film) {
            return (new FilmResource($film));
        });

        return parent::toArray($request);
    }
}
