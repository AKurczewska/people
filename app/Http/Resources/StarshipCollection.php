<?php

namespace App\Http\Resources;

use App\Models\Starship;
use Illuminate\Http\Resources\Json\ResourceCollection;

class StarshipCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Starship $starship) {
            return (new StarshipResource($starship));
        });

        return parent::toArray($request);
    }
}
