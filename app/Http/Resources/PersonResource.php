<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PersonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'height' => $this->height,
            'mass' => $this->mass,
            'hair_color' => $this->hair_color,
            'skin_color' => $this->skin_color,
            'eye_color' => $this->eye_color,
            'birth_year' => $this->birth_year,
            'gender' => $this->gender,
            'homeworld' => $this->homeworld,
            'films' => new FilmCollection($this->films),
            'species' => new SpeciesCollection($this->species),
            'starships' => new StarshipCollection($this->starships),
            'vehicles' => new VehicleCollection($this->vehicles),
            'created' => $this->created,
            'edited' => $this->edited,
            'url' => $this->url,
        ];
    }
}
