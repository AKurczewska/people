<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\UserStoreRequest;
use App\Http\Services\AuthService;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function register(UserStoreRequest $request)
    {
        $token = $this->authService->register($request);
        return response()->json(['token' => $token], 200);
    }


    public function login(LoginRequest $request)
    {
        $token = $this->authService->login($request);
        return $token ? response()->json(['token' => $token], 200)
            : response()->json(['message' => 'Bad email or password!'], 401);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
}
