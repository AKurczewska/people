<?php

namespace App\Http\Controllers;

use App\Http\Requests\PersonShowRequest;
use App\Http\Resources\PersonResource;
use App\Http\Services\PersonService;

class PersonController extends Controller
{
    protected $personService;

    public function __construct(PersonService $personService)
    {
        $this->personService = $personService;
    }

    public function show(PersonShowRequest $request)
    {
        $person = $this->personService->personDataByName($request);

        return response()->json(new PersonResource($person));
    }
}
