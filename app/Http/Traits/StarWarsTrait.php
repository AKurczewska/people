<?php

namespace App\Http\Traits;

use App\Models\Film;
use App\Models\Person;
use App\Models\Species;
use App\Models\Starship;
use App\Models\Vehicle;
use Exception;
use GuzzleHttp\Client;

trait StarWarsTrait {

    protected $peopleUrl = 'people/?page=';

    public function getSwapiData()
    {
        $page = 1;
        $url = $this->peopleUrl . $page;

        $this->getPeople($page, $url);
    }

    private function getPeople(int $page, string $url)
    {
        try {
            $response = $this->getGuzzleRequest(env('SWAPI_URL') . $url);
        } catch (Exception $e) {
            $response = null;
        }
        if ($response) {
            $page++;
            foreach ($response->results as $person)
                $this->addPersonToDb($person);
            if ($response->next)
                $this->getPeople($page, $this->peopleUrl . $page);
        }
    }

    private function addPersonToDb(object $data)
    {
        $person = Person::create([
            'name' => $data->name,
            'height' => $data->height,
            'mass' => $data->mass,
            'hair_color' => $data->hair_color,
            'skin_color' => $data->skin_color,
            'eye_color' => $data->eye_color,
            'birth_year' => $data->birth_year,
            'gender' => $data->gender,
            'homeworld' => $this->getPlanet($data->homeworld),
            'created' => $data->created,
            'edited' => $data->edited,
            'url' => $data->url,
        ]);

        foreach ($data->films as $film) {
            $film = $this->getFilm($film);
            if ($film)
                $person->films()->attach($film->id);
        }

        foreach ($data->species as $species) {
            $species = $this->getSpecies($species);
            if ($species)
                $person->species()->attach($species->id);
        }

        foreach ($data->starships as $starship) {
            $starship = $this->getStarship($starship);
            if ($starship)
                $person->starships()->attach($starship->id);
        }

        foreach ($data->vehicles as $vehicle) {
            $vehicle = $this->getVehicle($vehicle);
            if ($vehicle)
                $person->vehicles()->attach($vehicle->id);
        }
    }

    private function getPlanet(string $url)
    {
        try {
            $response = $this->getGuzzleRequest($url);
        } catch (Exception $e) {
            $response = null;
        }

        return $response ? $response->name : 'null';
    }

    private function getFilm(string $url)
    {
        try {
            $response = $this->getGuzzleRequest($url);
        } catch (Exception $e) {
            $response = null;
        }

        if ($response) {
            $response = $this->addFilmToDb($response);
        }

        return $response;
    }

    private function addFilmToDb(object $data)
    {
        $film = Film::where('url', $data->url)->first();
        if (!$film)
            $film = Film::create([
                'title' => $data->title,
                'url' => $data->url,
            ]);

        return $film;
    }

    private function getSpecies(string $url)
    {
        try {
            $response = $this->getGuzzleRequest($url);
        } catch (Exception $e) {
            $response = null;
        }

        if ($response) {
            $response = $this->addSpeciesToDb($response);
        }

        return $response;
    }

    private function addSpeciesToDb(object $data)
    {
        $species = Species::where('url', $data->url)->first();
        if (!$species)
            $species = Species::create([
                'name' => $data->name,
                'url' => $data->url,
            ]);

        return $species;
    }

    private function getStarship(string $url)
    {
        try {
            $response = $this->getGuzzleRequest($url);
        } catch (Exception $e) {
            $response = null;
        }

        if ($response) {
            $response = $this->addStarshipToDb($response);
        }

        return $response;
    }

    private function addStarshipToDb(object $data)
    {
        $starship = Starship::where('url', $data->url)->first();
        if (!$starship)
            $starship = Starship::create([
                'name' => $data->name,
                'url' => $data->url,
            ]);

        return $starship;
    }

    private function getVehicle(string $url)
    {
        try {
            $response = $this->getGuzzleRequest($url);
        } catch (Exception $e) {
            $response = null;
        }

        if ($response) {
            $response = $this->addVehicleToDb($response);
        }

        return $response;
    }

    private function addVehicleToDb(object $data)
    {
        $vehicle = Vehicle::where('url', $data->url)->first();
        if (!$vehicle)
            $vehicle = Vehicle::create([
                'name' => $data->name,
                'url' => $data->url,
            ]);

        return $vehicle;
    }

    private function getGuzzleRequest($url)
    {
        $client = new Client();

        $response = $client->get($url);

        return json_decode($response->getBody());
    }
}
